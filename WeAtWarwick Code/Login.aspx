﻿<%@ Page Title="Login" Language="VB" AutoEventWireup="false" Inherits="Login" CodeFile="Login.aspx.vb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Warwick Social Network</title>

    <!-- Browser icon -->
    <link rel="shortcut icon" href="Pictures/favicon.ico">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/login.css" type="text/css">
</head>

<!--Design!!!!!!! https://bootsnipp.com/snippets/QANqX -->
<body>

    <!-- Top nav -->
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a class="navbar-brand" href="#">We@Warwick</a></li>
            </ul>
        </div>
    </nav>
    <!-- Nav End -->

    <!-- Background img -->
    <div class="hero-image">
        <div class="container" style="padding-top: 10%;">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-login">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="#" class="active" id="login-form-link">Login</a>
                                </div>
                                <div class="col-xs-6">
                                    <asp:HyperLink ID="HyperLinkRegistration" runat="server" NavigateUrl="~/Registration.aspx">Registration</asp:HyperLink>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="form" runat="server" style="display: block;">
                                        <div class="form-group">
                                            <asp:TextBox ID="TextBoxEmail" runat="server" class="form-control" placeholder="Your Warwick Email Adress :)"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password" class="form-control" placeholder="Password"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <asp:Button ID="ButtonLogin" runat="server" OnClick="Login_Click" Text="Login" class="form-control btn btn-login" />
                                                </div>
                                                <!-- <h3 style="text-align: center;">We@Warwick is a Social Media platform dedicated to students of the Warwick University</h3>-->
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>

