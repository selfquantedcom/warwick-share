﻿Imports System.Data.OleDb

Partial Class Network
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Establish Db connection 
        Dim myconnection = CType(Master, MasterPage).EstablishDbConn()
        myconnection.Open()

        'Establish variables, queries and commands
        Dim userID As String = CType(Master, MasterPage).GetUserID()
        Dim usersTable As New Table
        Dim friendCond As Boolean = False
        Dim friendReqReceivedCond As Boolean = False
        Dim friendReqSubmitedCond As Boolean = False

        Dim getUsersQry As String = "SELECT * FROM [users]"
        Dim getUserscmd As New OleDbCommand(getUsersQry, myconnection)
        Dim readerUsers As OleDbDataReader = getUserscmd.ExecuteReader()

        'Generate table with all users 
        While readerUsers.Read()

            Dim DataRow As New TableRow
            DataRow.BorderWidth = 1
            Dim profilePic As New Image
            profilePic.Width = 80
            profilePic.Height = 80
            Dim detail As New Button
            detail.CssClass = "btn btn-info"

            Dim addFriend As New Button
            addFriend.CssClass = "btn btn-default"
            Dim removeFriend As New Button
            removeFriend.CssClass = "btn btn-danger"
            Dim acceptFriend As New Button
            acceptFriend.CssClass = "btn btn-success"

            DataRow.TableSection = TableRowSection.TableBody
            Dim DataCell1, DataCell2, DataCell3, DataCell4, DataCell6 As New TableCell

            DataCell1.BackColor = Drawing.Color.Azure
            DataCell2.BackColor = Drawing.Color.Azure
            DataCell3.BackColor = Drawing.Color.Azure
            DataCell4.BackColor = Drawing.Color.Azure
            DataCell6.BackColor = Drawing.Color.Azure

            'Get friend, not friend, me - status. Add buttons "add friend/remove friend"
            Dim getFriednsQry As String = "SELECT * FROM [friends]"
            Dim getFriendscmd As New OleDbCommand(getFriednsQry, myconnection)
            Dim readerFriends As OleDbDataReader = getFriendscmd.ExecuteReader()

            While readerFriends.Read()
                'is the user a friend?
                If (readerUsers(0) = readerFriends(1)) Then
                    If (readerFriends(2) = userID) Then
                        If (readerFriends(3) = True) Then
                            friendCond = True
                        End If
                    End If
                End If
                If (readerUsers(0) = readerFriends(2)) Then
                    If (readerFriends(1) = userID) Then
                        If (readerFriends(3) = True) Then
                            friendCond = True
                        End If
                    End If
                End If

                'Not friend but the current user has already submited a friend request
                If (readerUsers(0) = readerFriends(1)) Then
                    If (readerFriends(2) = userID) Then
                        If (readerFriends(3) = False) Then
                            If (readerFriends(4) = True) Then
                                friendReqReceivedCond = True
                            End If
                        End If
                    End If
                End If

                'Not friend but the other user has sent a friend request
                If (readerUsers(0) = readerFriends(2)) Then
                    If (readerFriends(1) = userID) Then
                        If (readerFriends(3) = False) Then
                            If (readerFriends(4) = True) Then
                                friendReqSubmitedCond = True
                            End If
                        End If
                    End If
                End If

            End While
            readerFriends.Close()

            'when friend is found, retrieve information about him from users table
            If (readerUsers(0) = userID) Then
                DataCell1.BackColor = Drawing.Color.Khaki
                DataCell2.BackColor = Drawing.Color.Khaki
                DataCell3.BackColor = Drawing.Color.Khaki
                DataCell4.BackColor = Drawing.Color.Khaki
                DataCell6.BackColor = Drawing.Color.Khaki

            ElseIf (friendCond) Then
                'Remove friend button
                AddHandler removeFriend.Click, AddressOf Me.RemoveFriend_Click
                removeFriend.Text = "Remove friend"
                removeFriend.CommandName = readerUsers(0).ToString()
                DataCell6.Controls.Add(removeFriend)

            ElseIf (friendReqReceivedCond) Then
                'Accept friend request
                AddHandler acceptFriend.Click, AddressOf Me.AcceptFriend_Click
                acceptFriend.Text = "Accept request"
                acceptFriend.CommandName = readerUsers(0).ToString()
                DataCell6.Controls.Add(acceptFriend)

            ElseIf (friendReqSubmitedCond) Then
                DataCell6.Text = "Request submited"
                DataCell6.ForeColor = Drawing.Color.Green

            Else
                'Add friend button
                AddHandler addFriend.Click, AddressOf Me.AddFriend_Click
                addFriend.Text = "Add friend"
                addFriend.CommandName = readerUsers(0).ToString()
                DataCell6.Controls.Add(addFriend)

            End If

            friendCond = False
            friendReqSubmitedCond = False
            friendReqReceivedCond = False

            'Load picture
            profilePic.ImageUrl = readerUsers(8)

            DataCell1.Controls.Add(profilePic)
            DataCell2.Text = readerUsers(2).ToString()
            DataCell3.Text = readerUsers(3).ToString()

            'display info about user on click
            AddHandler detail.Click, AddressOf Me.Detail_Click
            detail.Text = "Detail"
            detail.CommandName = readerUsers(0).ToString()
            DataCell4.Controls.Add(detail)

            DataRow.Cells.Add(DataCell1)
            DataRow.Cells.Add(DataCell2)
            DataRow.Cells.Add(DataCell3)
            DataRow.Cells.Add(DataCell4)
            DataRow.Cells.Add(DataCell6)
            usersTable.Rows.Add(DataRow)

        End While
        readerUsers.Close()

        'Display list of users
        PanelUsers.Controls.Add(usersTable)

        '--------------------------------------------------
        'FRIENDS RECOMMENDED: Share at least one of the same interests with the user
        '-------------------------------------------------

        'To prevent multiple data retrieval. Keep track of retrieved users. Exclude Me from search results
        Dim retrievedUsers As New List(Of Integer)
        Dim userInterests As New List(Of Integer)
        retrievedUsers.Add(userID)
        Dim cond As Boolean

        'Get user ID of users which have any of the interests of the currently logged in user
        'Establish variables, queries and commands
        Dim usrTable As New Table

        Dim getUsers2Qry As String = "SELECT * FROM [users];"
        Dim getUsers2cmd As New OleDbCommand(getUsers2Qry, myconnection)
        Dim reader2Users As OleDbDataReader = getUsers2cmd.ExecuteReader()

        Dim getUserInterestsQry As String = "SELECT * FROM [user_interest];"
        Dim getUserInterestscmd As New OleDbCommand(getUserInterestsQry, myconnection)
        Dim readerUserInterests As OleDbDataReader = getUserInterestscmd.ExecuteReader()

        'Get interests of the current user. Store them in a list
        While readerUserInterests.Read()
            If (readerUserInterests(2) = userID) Then
                userInterests.Add(readerUserInterests(1))
            End If
        End While

        'Find users who have indicated either of the matching interests
        For i As Integer = 0 To (userInterests.Count() - 1)

            Dim interestID As Integer = userInterests(i)
            Dim getUserIDQry As String = "SELECT * FROM [user_interest];"
            Dim getUserIDcmd As New OleDbCommand(getUserIDQry, myconnection)
            Dim reader As OleDbDataReader = getUserIDcmd.ExecuteReader()

            While reader.Read()

                'Find IDs of users with matching interests
                If (reader(1) = interestID) Then
                    'Store ID of such users in a list to avoid duplicates
                    cond = retrievedUsers.Contains(Int(reader(2)))
                    If (cond = False) Then
                        retrievedUsers.Add(Int(reader(2)))
                    End If
                End If
            End While
            reader.Close()
        Next

        'Retrieve detailed information about users with matching interests. Exclude the user itself
        retrievedUsers.Remove(userID)

        'Display table with users 
        While reader2Users.Read()

            If (retrievedUsers.Contains(Int(reader2Users(0)))) Then
                Dim DataRow As New TableRow
                DataRow.BorderWidth = 1
                Dim profilePic As New Image
                profilePic.Width = 80
                profilePic.Height = 80
                Dim detail As New Button
                detail.CssClass = "btn btn-info"

                Dim addFriend As New Button
                addFriend.CssClass = "btn btn-default"
                Dim removeFriend As New Button
                removeFriend.CssClass = "btn btn-danger"
                Dim acceptFriend As New Button
                acceptFriend.CssClass = "btn btn-success"

                DataRow.TableSection = TableRowSection.TableBody
                Dim DataCell1, DataCell2, DataCell3, DataCell4, DataCell6 As New TableCell

                DataCell1.BackColor = Drawing.Color.Azure
                DataCell2.BackColor = Drawing.Color.Azure
                DataCell3.BackColor = Drawing.Color.Azure
                DataCell4.BackColor = Drawing.Color.Azure
                DataCell6.BackColor = Drawing.Color.Azure

                'Get friend, not friend, me - status. Add buttons "add friend/remove friend"
                Dim getFriednsQry As String = "SELECT * FROM [friends];"
                Dim getFriendscmd As New OleDbCommand(getFriednsQry, myconnection)
                Dim readerFriends As OleDbDataReader = getFriendscmd.ExecuteReader()

                While readerFriends.Read()
                    'is the user a friend?
                    If (reader2Users(0) = readerFriends(1)) Then
                        If (readerFriends(2) = userID) Then
                            If (readerFriends(3) = True) Then
                                friendCond = True
                            End If
                        End If
                    End If
                    If (reader2Users(0) = readerFriends(2)) Then
                        If (readerFriends(1) = userID) Then
                            If (readerFriends(3) = True) Then
                                friendCond = True
                            End If
                        End If
                    End If

                    'Not friend but the current user has already submited a friend request
                    If (reader2Users(0) = readerFriends(1)) Then
                        If (readerFriends(2) = userID) Then
                            If (readerFriends(3) = False) Then
                                If (readerFriends(4) = True) Then
                                    friendReqReceivedCond = True
                                End If
                            End If
                        End If
                    End If

                    'Not friend but the other user has sent a friend request
                    If (reader2Users(0) = readerFriends(2)) Then
                        If (readerFriends(1) = userID) Then
                            If (readerFriends(3) = False) Then
                                If (readerFriends(4) = True) Then
                                    friendReqSubmitedCond = True
                                End If
                            End If
                        End If
                    End If

                End While
                readerFriends.Close()

                'If friend, display 'remove friend' button
                If (friendCond) Then
                    'Remove friend button
                    AddHandler removeFriend.Click, AddressOf Me.RemoveFriend_Click
                    removeFriend.Text = "Remove friend"
                    removeFriend.CommandName = reader2Users(0).ToString()
                    DataCell6.Controls.Add(removeFriend)

                ElseIf (friendReqReceivedCond) Then
                    'Accept friend request
                    AddHandler acceptFriend.Click, AddressOf Me.AcceptFriend_Click
                    acceptFriend.Text = "Accept request"
                    acceptFriend.CommandName = reader2Users(0).ToString()
                    DataCell6.Controls.Add(acceptFriend)

                ElseIf (friendReqSubmitedCond) Then
                    DataCell6.Text = "Request submited"
                    DataCell6.ForeColor = Drawing.Color.Green

                Else
                    'Add friend button
                    AddHandler addFriend.Click, AddressOf Me.AddFriend_Click
                    addFriend.Text = "Add friend"
                    addFriend.CommandName = reader2Users(0).ToString()
                    DataCell6.Controls.Add(addFriend)

                End If
                friendCond = False
                friendReqSubmitedCond = False
                friendReqReceivedCond = False

                'Load picture
                profilePic.ImageUrl = reader2Users(8)

                DataCell1.Controls.Add(profilePic)
                DataCell2.Text = reader2Users(2).ToString()
                DataCell3.Text = reader2Users(3).ToString()

                'display info about user on click
                AddHandler detail.Click, AddressOf Me.Detail_Click
                detail.Text = "Detail"
                detail.CommandName = reader2Users(0).ToString()
                DataCell4.Controls.Add(detail)

                DataRow.Cells.Add(DataCell1)
                DataRow.Cells.Add(DataCell2)
                DataRow.Cells.Add(DataCell3)
                DataRow.Cells.Add(DataCell4)
                DataRow.Cells.Add(DataCell6)
                usrTable.Rows.Add(DataRow)

            End If
        End While

        reader2Users.Close()
        myconnection.Close()

        'Display list of users with shared interests
        PanelRecommendedFriends.Controls.Add(usrTable)


    End Sub

    'Handle search by interest
    Protected Sub ButtonSubmit_Click(sender As Object, e As EventArgs) Handles ButtonSubmit.Click

        'Establish Db connection
        Dim myconnection = CType(Master, MasterPage).EstablishDbConn()
        myconnection.Open()

        'To prevent multiple data retrieval. Keep track of retrieved users. Exclude Me from search results
        Dim userID As String = CType(Master, MasterPage).GetUserID()
        Dim retrievedUsers As New List(Of Integer)
        retrievedUsers.Add(userID)
        Dim cond As Boolean

        'Get user ID of users which have indicated the same interests. Store them in a list

        'Establish variables, queries and commands
        Dim usersTable As New Table

        Dim getUsersQry As String = "SELECT * FROM [users];"
        Dim getUserscmd As New OleDbCommand(getUsersQry, myconnection)
        Dim readerUsers As OleDbDataReader = getUserscmd.ExecuteReader()

        For Each item In ListBoxSearch.Items
            If (item.Selected) Then

                Dim interestID As Integer = Int(item.value.ToString())
                'Get ID of appropriate users from user_interest table
                Dim getUserIDQry As String = "SELECT * FROM [user_interest];"
                Dim getUserIDcmd As New OleDbCommand(getUserIDQry, myconnection)
                Dim reader As OleDbDataReader = getUserIDcmd.ExecuteReader()

                While reader.Read()

                    'Find users with matching interests
                    If (reader(1) = interestID) Then
                        'Store ID of such users in a list. Avoid duplicates
                        cond = retrievedUsers.Contains(Int(reader(2)))
                        If (cond = False) Then
                            retrievedUsers.Add(Int(reader(2)))
                        End If
                    End If
                End While
            End If
        Next

        'Retrieve detailed information about users which indicated some of the selected interests

        'Remove the current user itself so that he doesnt appear in search results
        retrievedUsers.Remove(userID)

        'Display table with users 
        While readerUsers.Read()

            If (retrievedUsers.Contains(Int(readerUsers(0)))) Then
                Dim DataRow As New TableRow
                DataRow.BorderWidth = 1
                Dim profilePic As New Image
                profilePic.Width = 80
                profilePic.Height = 80

                DataRow.TableSection = TableRowSection.TableBody
                Dim DataCell1, DataCell2, DataCell3 As New TableCell

                DataCell1.BackColor = Drawing.Color.Azure
                DataCell2.BackColor = Drawing.Color.Azure
                DataCell3.BackColor = Drawing.Color.Azure

                'Load picture
                profilePic.ImageUrl = readerUsers(8)

                DataCell1.Controls.Add(profilePic)
                DataCell2.Text = readerUsers(2).ToString()
                DataCell3.Text = readerUsers(3).ToString()

                DataRow.Cells.Add(DataCell1)
                DataRow.Cells.Add(DataCell2)
                DataRow.Cells.Add(DataCell3)
                usersTable.Rows.Add(DataRow)
            End If
        End While

        readerUsers.Close()
        myconnection.Close()

        'Display list of users with either of the selected interests 
        PanelInterestFriends.Controls.Add(usersTable)


    End Sub

    'Handler for accept friend button
    Private Sub AcceptFriend_Click(sender As Object, e As EventArgs)
        Dim myconnection = CType(Master, MasterPage).EstablishDbConn()
        myconnection.Open()

        'Establish variables, queries and commands
        Dim userID As String = CType(Master, MasterPage).GetUserID()
        Dim btn As Button = CType(sender, Button)
        Dim friendID As Integer = Int(btn.CommandName)

        Dim updateFriendRecordQry As String = "UPDATE friends SET friend_request_acceptance = True, request_sent = False WHERE (friend_request_id = " & friendID & " AND friend_recipient_id = " & userID & ") OR (friend_recipient_id = " & friendID & " AND friend_request_id = " & userID & ");"
        Dim updateFriendRecordcmd As New OleDbCommand(updateFriendRecordQry, myconnection)
        updateFriendRecordcmd.ExecuteNonQuery()

        myconnection.Close()
        Response.Redirect(Request.RawUrl)

    End Sub

    'add friend button handler
    Private Sub AddFriend_Click(sender As Object, e As EventArgs)
        'Establish Db connection
        Dim myconnection = CType(Master, MasterPage).EstablishDbConn()
        myconnection.Open()

        'Establish variables, queries and commands
        Dim userID As String = CType(Master, MasterPage).GetUserID()
        Dim btn As Button = CType(sender, Button)

        Dim friendID As Integer = Int(btn.CommandName)
        Dim currentDate As DateTime = DateTime.Today.ToString("dd/MM/yyyy")
        Dim currentTime As DateTime = DateTime.Now.ToString("HH:mm")

        'Verify is friend record already exists in table friends
        Dim verifyEntryQry As String = "Select COUNT(*) FROM friends WHERE (friend_request_id = " & friendID & " And friend_recipient_id = " & userID & ") Or (friend_recipient_id = " & friendID & " And friend_request_id = " & userID & ");"
        Dim verifyEntrycmd As New OleDbCommand(verifyEntryQry, myconnection)

        'If so, Update record 
        If (Int(verifyEntrycmd.ExecuteScalar()) > 0) Then
            Dim updateFriendRecordQry As String = "UPDATE friends Set friend_request_id = " & userID & ", friend_recipient_id = " & friendID & ",  friend_request_acceptance = False, request_sent = True WHERE (friend_request_id = " & friendID & " AND friend_recipient_id = " & userID & ") OR (friend_recipient_id = " & friendID & " AND friend_request_id = " & userID & ");"
            Dim updateFriendRecordcmd As New OleDbCommand(updateFriendRecordQry, myconnection)
            updateFriendRecordcmd.ExecuteNonQuery()

            'Otherwise create new record
        Else 'friend_acceptance_date, friend_request_acceptance, @accDate, , @accTime)
            Dim insertFriendRecordQry As String = "INSERT INTO [friends] (friend_request_id, friend_recipient_id, friend_request_acceptance, request_sent) VALUES(@userID, @friendID, @reqAcc, @reqSent);"
            Dim insertFriendRecordcmd As New OleDbCommand(insertFriendRecordQry, myconnection)
            insertFriendRecordcmd.Parameters.AddWithValue("@userID", userID)
            insertFriendRecordcmd.Parameters.AddWithValue("@friendID", friendID)
            insertFriendRecordcmd.Parameters.AddWithValue("@reqAcc", False)
            insertFriendRecordcmd.Parameters.AddWithValue("@reqSent", True)
            insertFriendRecordcmd.ExecuteNonQuery()
        End If

        myconnection.Close()
        Response.Redirect(Request.RawUrl)
    End Sub

    'Handler for detail's button
    Private Sub Detail_Click(sender As Object, e As EventArgs)
        'Display details view
        Dim btn As Button = CType(sender, Button)
        CType(Master, MasterPage).GenerateDetailsView(PanelDetail, btn)
    End Sub

    'Remove friend button handler 
    Private Sub RemoveFriend_Click(sender As Object, e As EventArgs)
        Dim btn As Button = CType(sender, Button)
        CType(Master, MasterPage).RemoveFriend(btn)
    End Sub

End Class