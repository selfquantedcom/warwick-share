﻿Imports System.Data.OleDb

Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Verifies user login 
        VerifyUser()

    End Sub

    'Handle navbar buttons
    Protected Sub LinkButtonNewsfeed_Click(sender As Object, e As EventArgs) Handles LinkButtonNewsfeed.Click
        'Go to Newsfeed page
        Dim userID As String = GetUserID()
        Response.Redirect("Newsfeed.aspx?sessionVar=" & userID)
    End Sub

    Protected Sub LinkButtonNetwork_Click(sender As Object, e As EventArgs) Handles LinkButtonNetwork.Click
        'Go to Network page
        Dim userID As String = GetUserID()
        Response.Redirect("Network.aspx?sessionVar=" & userID)
    End Sub

    Protected Sub LinkButtonSettings_Click(sender As Object, e As EventArgs) Handles LinkButtonSettings.Click
        'Go to Settings page, about you subpage by default
        Dim userID As String = GetUserID()
        Response.Redirect("SettingsAbout.aspx?sessionVar=" & userID)
    End Sub

    Protected Sub LinkButtonLogout_Click(sender As Object, e As EventArgs) Handles LinkButtonLogout.Click
        'Logout
        Dim sessionVar As String
        sessionVar = Request.QueryString("sessionVar").ToString()
        Session(sessionVar) = ""
        Response.Redirect("Login.aspx")
    End Sub

    'Functions

    'Verify user
    Public Function VerifyUser() As Object
        Dim sessionVar As String
        Try
            sessionVar = Request.QueryString("sessionVar").ToString()
            If (Session(sessionVar) = sessionVar) Then
            Else
                Response.Redirect("Login.aspx")
            End If
        Catch ex As Exception
            Response.Redirect("Login.aspx")
        End Try
        Return vbNull
    End Function

    'Establish connection with a database
    Public Function EstablishDbConn() As Object
        Dim constring As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Request.PhysicalApplicationPath.ToString() & "/App_Data/1419631.mdb"
        Return New OleDbConnection(constring)
    End Function

    'Get user's ID from session variable
    Public Function GetUserID() As Object
        Try
            Dim sessionVar As String = Request.QueryString("sessionVar").ToString()
            Return sessionVar
        Catch ex As Exception
            Return 0
        End Try
    End Function

    'Generate detailed information about a user and display it in a panel
    Public Function GenerateDetailsView(pnl As Panel, btn As Button) As Object
        'Establish Db connection
        Dim myconnection = EstablishDbConn()
        myconnection.Open()

        'Establish variables, queries and commands
        Dim userID As String = GetUserID()
        Dim profilePic As New Image
        Dim friendsTable, detailsTable As New Table



        Dim DataRow1, DataRow2, DataRow3, DataRow4, DataRow5, DataRow6, DataRow7, DataRow8 As New TableRow
        Dim DataCell1, DataCell2, DataCell3, DataCell4, DataCell5, DataCell6, DataCell7, DataCell8 As New TableCell
        DataRow1.TableSection = TableRowSection.TableBody
        DataRow2.TableSection = TableRowSection.TableBody
        DataRow4.TableSection = TableRowSection.TableBody
        DataRow5.TableSection = TableRowSection.TableBody
        DataRow6.TableSection = TableRowSection.TableBody
        DataRow7.TableSection = TableRowSection.TableBody
        DataRow8.TableSection = TableRowSection.TableBody

        Dim getFriendsInfoQry As String = "SELECT * FROM [users]"
        Dim getFriendsInfocmd As New OleDbCommand(getFriendsInfoQry, myconnection)
        Dim reader As OleDbDataReader = getFriendsInfocmd.ExecuteReader()

        'Display list of user's friends
        While reader.Read()
            'get friends id from the button 
            Dim friendID As Integer = Int(btn.CommandName)

            If (reader(0) = Int(btn.CommandName)) Then

                'Title
                DataCell3.Text = "Details View"
                DataCell3.Font.Bold = True

                'Display detailed info about a particular friend
                'load picture
                profilePic.ImageUrl = reader(8)
                profilePic.Width = 80
                profilePic.Height = 80

                'Information about user from the users rable
                DataCell1.Controls.Add(profilePic)
                DataCell2.Text = reader(2).ToString() & " " & reader(3).ToString()
                DataCell4.Text = reader(4).ToString()
                DataCell5.Text = "Date of Birth: " & reader(7).ToString()

                'Information about user from course table 
                Dim getCourseNameQry As String = "SELECT * FROM [courses];"
                Dim getCourseNamecmd As New OleDbCommand(getCourseNameQry, myconnection)
                Dim readerCourse As OleDbDataReader = getCourseNamecmd.ExecuteReader()

                While readerCourse.Read()
                    If (readerCourse(0) = reader(1)) Then
                        DataCell6.Text = "Course: " & readerCourse(1).ToString()
                    End If
                End While
                readerCourse.Close()

                'Information about user from interests and user_imterest tables
                Dim getUserInterestQry As String = "SELECT * FROM [user_interest];"
                Dim getUserInterestcmd As New OleDbCommand(getUserInterestQry, myconnection)
                Dim readerUserInterest As OleDbDataReader = getUserInterestcmd.ExecuteReader()
                Dim holder As String = "Interests: "

                While readerUserInterest.Read()

                    If (readerUserInterest(2) = Int(btn.CommandName)) Then
                        Dim getInterestNameQry As String = "SELECT * FROM [interests];"
                        Dim getInterestNamecmd As New OleDbCommand(getInterestNameQry, myconnection)
                        Dim readerInterestName As OleDbDataReader = getInterestNamecmd.ExecuteReader()

                        readerInterestName.Read()
                        While readerInterestName.Read()

                            If (readerInterestName(0) = readerUserInterest(1)) Then
                                holder = holder & readerInterestName(1).ToString() & " "
                            End If
                        End While
                        readerInterestName.Close()
                    End If

                End While
                readerUserInterest.Close()

                'Show user interests to the screen. 
                DataCell7.Text = holder

                'Generate HTML button which closes detail view
                Dim HTMLbutton As String = "<button onclick = 'hideDetail(this); return false;' Class='btn btn-default'>Close</button>"
                DataCell8.Controls.Add(New LiteralControl(HTMLbutton))

                'Input rows into table
                DataRow3.Cells.Add(DataCell3)
                friendsTable.Rows.Add(DataRow3)

                DataRow1.Cells.Add(DataCell1)
                friendsTable.Rows.Add(DataRow1)

                DataRow2.Cells.Add(DataCell2)
                friendsTable.Rows.Add(DataRow2)

                DataRow4.Cells.Add(DataCell4)
                friendsTable.Rows.Add(DataRow4)

                DataRow5.Cells.Add(DataCell5)
                friendsTable.Rows.Add(DataRow5)

                DataRow6.Cells.Add(DataCell6)
                friendsTable.Rows.Add(DataRow6)

                DataRow7.Cells.Add(DataCell7)
                friendsTable.Rows.Add(DataRow7)

                DataRow8.Cells.Add(DataCell8)
                friendsTable.Rows.Add(DataRow8)

                'styling
                DataCell1.BackColor = Drawing.Color.Khaki
                DataCell2.BackColor = Drawing.Color.Khaki
                DataCell3.BackColor = Drawing.Color.Khaki
                DataCell4.BackColor = Drawing.Color.Khaki
                DataCell5.BackColor = Drawing.Color.Khaki
                DataCell6.BackColor = Drawing.Color.Khaki
                DataCell7.BackColor = Drawing.Color.Khaki
                DataCell8.BackColor = Drawing.Color.Khaki

                'detailsTable.Style.Ad
                pnl.Controls.Add(friendsTable)
            End If
        End While
        reader.Close()
        myconnection.Close()

        Return 0
    End Function

    'Remove friend
    Public Function RemoveFriend(btn As Button) As Object

        'Establish Db connection
        Dim myconnection = EstablishDbConn()
        myconnection.Open()

        'Establish variables, queries and commands
        Dim userID As String = GetUserID()
        Dim friendID As Integer = Int(btn.CommandName)

        Dim removeFriendQry As String = "UPDATE friends SET friend_request_acceptance = False WHERE (friend_request_id = " & friendID & " AND friend_recipient_id = " & userID & ") OR (friend_recipient_id = " & friendID & " AND friend_request_id = " & userID & ");"
        Dim removeFriendcmd As New OleDbCommand(removeFriendQry, myconnection)
        removeFriendcmd.ExecuteNonQuery()

        myconnection.Close()
        Response.Redirect(Request.RawUrl)
        Return 0
    End Function

End Class


