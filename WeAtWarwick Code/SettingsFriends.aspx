﻿<%@ Page Title="Settings Friends" Language="VB" AutoEventWireup="false" MasterPageFile="~/Master.Master" CodeFile="SettingsFriends.aspx.vb" Inherits="SettingsFriends" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <div class="container">

        <!-- Heading-->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4">
                <h1 style="margin-left: 5px; text-align: left;">Settings page</h1>
            </div>
        </div>

        <!-- Sub page buttons-->
        <div class="row">
            <div class="form-group col-xs-12 col-sm-12 col-md-11">
                <asp:Button ID="ButtonAboutYou" class="btn btn-info  col-xs-12 col-sm-12 col-md-2" runat="server" Text="About Me" Style="display: inline-block; margin-left: 5px; margin-right: 5px; margin-bottom: 5px;" />
                <asp:Button ID="ButtonMyFriends" class="btn btn-info active col-xs-12 col-sm-12 col-md-2" runat="server" Text="My Friends" Style="display: inline-block; margin-left: 5px;" />
            </div>
        </div>

        <!--Display friends-->
        <div class="row col-xs-12 col-sm-12 col-md-12">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <h3>
                    <asp:Label ID="LabelFriends" runat="server" Text="Your Friends"></asp:Label></h3>
                <asp:Panel ID="PanelFriends" runat="server" Style="padding-left: -10px;"></asp:Panel>

            </div>

            <!--Detail view-->
            <div class="col-md-2"></div>
            <div class="col-xs-12 col-sm-12 col-md-4">
                <asp:Panel ID="PanelDetail" runat="server" Style="padding-top: 80px;"></asp:Panel>                
            </div>
        </div>
    </div>


</asp:Content>
