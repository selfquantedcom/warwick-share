﻿<%@ Page Title="Newsfeed" Language="VB" AutoEventWireup="false" MasterPageFile="~/Master.Master" CodeFile="Newsfeed.aspx.vb" Inherits="Newsfeed" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <!-- Custom CSS 
    <link rel="stylesheet" href="css/styling.css" type="text/css">-->

    <div class="container">

        <div class="col-xs-12 col-sm-12 col-md-12" style="text-align: center;">
            <h1>Newsfeed</h1>            
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <asp:Image ID="ImageProfilePic" runat="server" Style="display: block; margin-left: auto; margin-right: auto;" Height="100px" />
            <h3>
                <asp:Label ID="LabelUserNameSurname" runat="server" Text="Label" Style="display: block; margin-left: auto; margin-right: auto; text-align: center;"></asp:Label></h3>
        </div>

        <asp:TextBox ID="TextBoxComposePost" class="form-control" runat="server" BorderStyle="Solid" BorderWidth="1px" placeholder="What's on your mind?" Style="text-align: center; margin-left: 5px;" Height="100px"></asp:TextBox>
        <div class="col-sm-3 col-md-5"></div>

        <asp:Button ID="ButtonPost" class="btn btn-success" Style="margin-left: 5px; margin-top: 5px; margin-bottom: 5px; width: 180px; height: 40px;" runat="server" Text="Post" />




        <!--NESTED GRID VIEW:Posts and comments-->
        <div class="col-xs-12">

            <asp:ScriptManager ID="ScriptManagerNewsfeed" runat="server">
            </asp:ScriptManager>
            <asp:AccessDataSource ID="AccessDataSourcePosts" runat="server" SelectCommand="SELECT [post_content], [post_time], [post_date], [ID], [user_id] FROM [posts] ORDER BY post_date DESC, post_time DESC;" DataFile="~/App_Data/1419631.mdb"></asp:AccessDataSource>
            <asp:GridView ID="GridViewPosts" runat="server" AutoGenerateColumns="False" DataSourceID="AccessDataSourcePosts" Style="margin-left: 0px; margin-top: 10px;" DataKeyNames="ID" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
                <AlternatingRowStyle BackColor="#DCDCDC" />
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="post_date" />
                    <asp:BoundField DataField="user_id" HeaderText="user_id" SortExpression="user_id" />
                    <asp:BoundField DataField="post_date" HeaderText="post_date" SortExpression="post_date" />
                    <asp:BoundField DataField="post_time" HeaderText="post_time" SortExpression="post_date" />
                    <asp:BoundField DataField="post_content" HeaderText="post_content" SortExpression="post_date" />
                    <asp:TemplateField HeaderText="Comments">
                        <ItemTemplate>
                            <asp:GridView ID="GridViewComments" runat="server">
                                <Columns>
                                    <asp:BoundField DataField="comment" HeaderText="Comments" />
                                </Columns>
                            </asp:GridView>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Add comment">
                        <ItemTemplate>
                            <asp:TextBox ID="TextBoxComment" runat="server"></asp:TextBox>
                            <asp:Button ID="ButtonSubmitComment" runat="server" Text="Submit comment" CommandArgument='<% #Eval("ID") %>' OnClick="ButtonSubmitComment_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#000065" />
            </asp:GridView>
        </div>
    </div>

</asp:Content>
