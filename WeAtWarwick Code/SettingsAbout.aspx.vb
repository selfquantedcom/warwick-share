﻿Imports System.Data
Imports System.Data.OleDb
Imports System.IO

Partial Class SettingsAbout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        'Inform about user made changes
        If (Session("changePswd") = "Password has been changed!") Then
            LabelInfoChanged.Text = Session("changePswd")
        Else
            LabelChangePassword.Text = Session("changePswd")
        End If
        Session("changePswd") = ""
        LabelInfoChanged.Text = Session("infoChange")
        Session("infoChange") = ""

        If (Page.IsPostBack = False) Then
            'establish database connection
            Dim myconnection = CType(Master, MasterPage).EstablishDbConn()
            myconnection.Open()

            'Establish variables, queries and commands
            Dim userID As String = CType(Master, MasterPage).GetUserID()

            Dim userValueQry As String = "SELECT * FROM [users];"
            Dim userValuecmd As New OleDbCommand(userValueQry, myconnection)
            Dim reader As OleDbDataReader = userValuecmd.ExecuteReader()

            'In user input fields, display values specific to the user
            While reader.Read()
                If (reader(0).ToString() = userID) Then
                    FirstName.Text = reader(2).ToString()
                    LastName.Text = reader(3).ToString()
                    DropDownListPrivacy.SelectedValue = reader(6)
                    DropDownListCourse.SelectedValue = reader(1)
                    ImageProfile.ImageUrl = reader(8).ToString()
                End If
            End While
            reader.Close()

            myconnection.Close()
        End If

    End Sub

    'Commit user made changes to database
    Protected Sub ButtonSubmit_Click(sender As Object, e As EventArgs) Handles ButtonSubmit.Click

        'establish database connection
        Dim myconnection = CType(Master, MasterPage).EstablishDbConn()
        myconnection.Open()

        Dim userID As String = CType(Master, MasterPage).GetUserID()

        Dim userValueQry As String = "SELECT * FROM [users];"
        Dim userValuecmd As New OleDbCommand(userValueQry, myconnection)
        Dim reader As OleDbDataReader = userValuecmd.ExecuteReader()

        'Upload picture
        If (FileUploadPicture.HasFile) Then
            Try
                'Upload file to the server
                Dim filename As String = Path.GetFileName(FileUploadPicture.FileName)
                filename = filename.Trim()
                FileUploadPicture.SaveAs(Server.MapPath("~\\") + "/Pictures/" + filename)
                Dim picReference As String = "/Pictures/" + filename

                'Profile pic Qry
                Dim uploadPicQry As String = "UPDATE users SET picture_url = '" & picReference & "' WHERE id = " & userID & ";"
                Dim uploadPiccmd As New OleDbCommand(uploadPicQry, myconnection)

                uploadPiccmd.ExecuteNonQuery()

                'Clear file upload hnadler
                FileUploadPicture.PostedFile.InputStream.Dispose()

            Catch ex As Exception
            End Try
        End If

        'Establish variables, queries and commands
        Dim privacySetting As Boolean = DropDownListPrivacy.SelectedValue
        Dim courseID As Integer = DropDownListCourse.SelectedValue

        'Get current date of birth if unchanged
        If (TextBoxCalendar.Text = "") Then
            Dim userDOBQry As String = "SELECT * FROM [users];"
            Dim userDOBcmd As New OleDbCommand(userDOBQry, myconnection)
            Dim readerDOB As OleDbDataReader = userDOBcmd.ExecuteReader()

            While readerDOB.Read()
                If (readerDOB(0).ToString() = userID) Then
                    TextBoxCalendar.Text = readerDOB(7).ToString()
                End If
            End While
            readerDOB.Close()
        End If

        'Commit user made changes to users table
        Dim updateQry As String
        updateQry = "UPDATE [users] SET course_id = " & courseID & ", user_firstname = '" & FirstName.Text & "', user_lastname = '" & LastName.Text & "', public_privacy = " & privacySetting & ", DOB = '" & TextBoxCalendar.Text & "' WHERE id = " & userID & ";"
        Dim userscmd As New OleDbCommand(updateQry, myconnection)
        userscmd.ExecuteNonQuery()

        'Delete prior interests from users_interests table
        If (ListBoxInterests.SelectedIndex <> -1) Then
            Dim deleteQry As String
            deleteQry = "DELETE * FROM [user_interest] WHERE [user_interest].[user_id]=" & userID & ";"
            Dim deletecmd As New OleDbCommand(deleteQry, myconnection)
            deletecmd.ExecuteNonQuery()

            'Add newly added interests into table user_interests
            Dim interestQry, userInterestsqlQry As String
            Dim interestId As Integer
            For Each item In ListBoxInterests.Items
                'get interest_id
                If (item.Selected) Then
                    interestQry = "SELECT [interests].[ID] FROM [interests] WHERE [interests].[interests]='" & item.ToString() & "';"
                    Dim interestcmd As New OleDbCommand(interestQry, myconnection)
                    interestId = Int(interestcmd.ExecuteScalar().ToString())

                    'Insert items into table users_interests 
                    userInterestsqlQry = "INSERT INTO [user_interest] (interests_id, user_id) VALUES (@interestID, @usrID);"
                    Dim userInterestcmd As New OleDb.OleDbCommand(userInterestsqlQry, myconnection)

                    userInterestcmd.Parameters.AddWithValue("@interestID", interestId)
                    userInterestcmd.Parameters.AddWithValue("@usrID", userID)
                    userInterestcmd.ExecuteNonQuery()

                End If
            Next
        End If
        myconnection.Close()
        Session("infoChange") = "Your information has been updated"
        Response.Redirect(Request.RawUrl)

    End Sub

    'Handle password change in modal 
    Protected Sub ButtonChangePassword_Click(sender As Object, e As EventArgs) Handles ButtonChangePassword.Click

        'establish database connection
        Dim myconnection = CType(Master, MasterPage).EstablishDbConn()
        myconnection.Open()

        'Establish variables, queries and commands
        Dim userID As String = CType(Master, MasterPage).GetUserID()
        Dim databasePswd As String

        Dim userPswdQry As String = "SELECT * FROM [users]"
        Dim verifyPswdcmd As New OleDbCommand(userPswdQry, myconnection)
        Dim reader As OleDbDataReader = verifyPswdcmd.ExecuteReader()

        'Get existing user password
        While reader.Read()
            If (reader(0).ToString() = userID) Then
                databasePswd = reader(6).ToString()
            End If
        End While
        reader.Close()

        'Confirm that Db password matches usr input
        If (databasePswd = TextBoxCurrentPswd.Text) Then
            If (TextBoxNewpswd.Text.Length < 8) Then
                'Change password
                Dim changePswdQry As String = "UPDATE users SET user_password ='" & TextBoxNewpswd.Text & "' WHERE id = " & userID & ";"
                Dim changePswdcmd As New OleDbCommand(changePswdQry, myconnection)
                changePswdcmd.ExecuteNonQuery()

                'Clear input, notify user of changes and refresh
                TextBoxCurrentPswd.Text = ""
                TextBoxNewpswd.Text = ""
                Session("changePswd") = "New password must be at least 8 characters long. Password has not been changed!"
                Response.Redirect(Request.RawUrl)
            Else
                'Clear input, notify user of changes and refresh
                TextBoxCurrentPswd.Text = ""
                TextBoxNewpswd.Text = ""
                Session("changePswd") = "Password has been changed!"
                Response.Redirect(Request.RawUrl)
            End If
        Else
            'Clear input, notify user of changes and refresh
            TextBoxCurrentPswd.Text = ""
            TextBoxNewpswd.Text = ""
            Session("changePswd") = "Your current password didn't match your input. Password has not been changed!"
            Response.Redirect(Request.RawUrl)
        End If

        myconnection.Close()
    End Sub

    'Subpage buttons
    Protected Sub ButtonAboutYou_Click(sender As Object, e As EventArgs) Handles ButtonAboutYou.Click
        'Go to Settings page, about you subpage
        Response.Redirect(Request.RawUrl)
    End Sub

    Protected Sub ButtonMyFriends_Click(sender As Object, e As EventArgs) Handles ButtonMyFriends.Click
        'Go to Settings page, My Friends subpage
        Dim userID As String = CType(Master, MasterPage).GetUserID()
        Response.Redirect("SettingsFriends.aspx?sessionVar=" & userID)
    End Sub
End Class