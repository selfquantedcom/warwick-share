﻿Imports System.Data.OleDb
Imports System.Drawing
Imports System.IO

Partial Class Registration
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Reset fields 
        Email.Text = ""
        Password.Text = ""
        FirstName.Text = ""
        LastName.Text = ""

        Email.BorderColor = Color.FromArgb(204, 204, 204)
        Password.BorderColor = Color.FromArgb(204, 204, 204)
        FirstName.BorderColor = Color.FromArgb(204, 204, 204)
        LastName.BorderColor = Color.FromArgb(204, 204, 204)
    End Sub

    'Handle user input
    Protected Sub Submit_Click(sender As Object, e As EventArgs) Handles Submit.Click

        'establish database connection - doesn't have master page hence cant import functions from there
        Dim constring As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Request.PhysicalApplicationPath.ToString() & "/App_Data/1419631.mdb"
        Dim myconnection = New OleDbConnection(constring)
        myconnection.Open()

        'Establish variables
        Dim userName As Boolean = True
        Dim userLastName As Boolean = True
        Dim userEmail As Boolean = True
        Dim userPswd As Boolean = True
        Dim userDob As Boolean = True
        Dim userCourse As Boolean = True
        Dim fName, lName, emailAdress, pswd, course As String
        Dim courseID, usrID, interestId, userExists As Integer
        Dim dateOfBirth As DateTime
        Dim defaultProfilePic As String = "/pictures/defaultProfilePic.png"

        'Field validators
        If (FirstName.Text = "") Then
            FirstName.BorderColor = Color.Red
            userName = False
        Else
            FirstName.BorderColor = Color.FromArgb(204, 204, 204)
            userName = True
        End If

        If (LastName.Text = "") Then
            LastName.BorderColor = Color.Red
            userLastName = False
        Else
            LastName.BorderColor = Color.FromArgb(204, 204, 204)
            userLastName = True
        End If

        If (Email.Text.Split("@").Last() <> "warwick.ac.uk") Then
            Email.BorderColor = Color.Red
            Email.Text = ""
            userEmail = False
        Else
            Email.BorderColor = Color.FromArgb(204, 204, 204)
            userEmail = True
        End If

        If (TextBoxCalendar.Text = "") Then
            TextBoxCalendar.BorderColor = Color.Red
            userDob = False
        Else
            TextBoxCalendar.BorderColor = Color.FromArgb(204, 204, 204)
            userDob = True
        End If

        If (DropDownListCourse.SelectedValue = 6) Then
            DropDownListCourse.BorderColor = Color.Red
            userCourse = False
        Else
            DropDownListCourse.BorderColor = Color.FromArgb(204, 204, 204)
            userCourse = True
        End If

        If (Password.Text.Length() < 8) Then
            Password.Text = ""
            Password.BorderColor = Color.Red
            userPswd = False
        Else
            Password.BorderColor = Color.FromArgb(204, 204, 204)
            userPswd = True
        End If

        'If validators satisfied, store user input in Db
        If (userName And userLastName And userEmail And userPswd And userDob And userCourse) Then

            'Match vars with user input
            emailAdress = Email.Text
            pswd = Password.Text
            fName = FirstName.Text
            lName = LastName.Text
            course = DropDownListCourse.SelectedValue.ToString()
            dateOfBirth = Convert.ToDateTime(TextBoxCalendar.Text)
            courseID = DropDownListCourse.SelectedValue

            'Establish sql queries and commands
            Dim IDsqlQry As String = "SELECT TOP 1 * FROM [users] ORDER BY [ID] DESC;"
            Dim verifyEmailsqlQry As String = "SELECT COUNT(*) FROM [users] WHERE [users].[user_email]='" & emailAdress & "';"
            Dim userssqlQry As String = "INSERT INTO [users]([course_id], [user_firstname], [user_lastname], [user_email], [public_privacy], [user_password], [DOB], [picture_url]) VALUES(@courseID, @firstName, @lastName, @email, false, @password, @dateOfBirth, @defaultPic);"
            Dim userInterestsqlQry As String = "INSERT INTO [user_interest]([interests_id], [user_id]) VALUES(@interId, @usrID);"


            Dim IDcmd As New OleDbCommand(IDsqlQry, myconnection)
            Dim userscmd As New OleDbCommand(userssqlQry, myconnection)
            Dim userInterestcmd As New OleDbCommand(userInterestsqlQry, myconnection)
            Dim verifyEmailcmd As New OleDbCommand(verifyEmailsqlQry, myconnection)

            'Get user ID
            usrID = Int(IDcmd.ExecuteScalar().ToString()) + 1
            'Verify unique email
            userExists = Int(verifyEmailcmd.ExecuteScalar().ToString())
            If (userExists = 0) Then
                'insert user defined values into the sql queries
                userscmd.Parameters.AddWithValue("@courseID", courseID)
                userscmd.Parameters.AddWithValue("@firstName", fName)
                userscmd.Parameters.AddWithValue("@lastName", lName)
                userscmd.Parameters.AddWithValue("@email", emailAdress)
                userscmd.Parameters.AddWithValue("@password", pswd)
                userscmd.Parameters.AddWithValue("@dateOfBirth", dateOfBirth)
                userscmd.Parameters.AddWithValue("@defaultPic", defaultProfilePic)

                'Insert valueS into table users
                userscmd.ExecuteNonQuery()

                'Add selected interests into table user_interests
                For Each item In ListBoxInterests.Items
                    If (item.Selected) Then
                        'insert user defined values into the sql queries
                        Dim interestQry As String = "SELECT [interests].[ID] FROM [interests] WHERE [interests].[interests] = '" & item.ToString() & "';"
                        Dim interestcmd As New OleDbCommand(interestQry, myconnection)
                        interestId = Int(interestcmd.ExecuteScalar().ToString())

                        userInterestcmd.Parameters.AddWithValue("@interID", interestId)
                        userInterestcmd.Parameters.AddWithValue("@usrID", usrID)
                        userInterestcmd.ExecuteNonQuery()

                    End If
                Next

                Response.Redirect("Login.aspx")
            Else
                Email.BorderColor = Color.Red
                Email.Text = "User email already registered"
                userEmail = False
            End If

        End If
        myconnection.Close()
    End Sub
End Class