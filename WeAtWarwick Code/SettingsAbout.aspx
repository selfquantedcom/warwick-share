﻿<%@ Page Title="Settings About" Language="VB" AutoEventWireup="false" MasterPageFile="~/Master.Master" CodeFile="SettingsAbout.aspx.vb" Inherits="SettingsAbout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
  
    <!-- Heading -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4">
                <h1 style="margin-left:5px; text-align:left;">Settings page</h1>
            </div>
        </div>

        <!-- Sub page buttons-->
        <div class="row">
            <div class="form-group col-xs-12 col-sm-12 col-md-8">
                <asp:Button ID="ButtonAboutYou" class="btn btn-info active col-xs-12 col-sm-12 col-md-2" runat="server" Text="About Me" Style="display: inline-block; margin-left:5px; margin-right: 5px; margin-bottom: 5px;" />
                <asp:Button ID="ButtonMyFriends" class="btn btn-info col-xs-12 col-sm-12 col-md-2" runat="server" Text="My Friends" Style="display: inline-block; margin-left:5px;" />
            </div>
        </div>

        <!-- Upload file-->
        <div class="col-xs-12 col-sm-12 col-md-3" style="padding: 0px;">
            <div class="form-group">
                <asp:Image ID="ImageProfile" runat="server" Height="200px" Width="200px" Style="display: block; margin-left: auto; margin-right: auto;" />
                <asp:FileUpload ID="FileUploadPicture" runat="server" Style="margin-top: 5px; display: block; margin-left: auto; margin-right: auto;" />
            </div>
        </div>

        <!-- User input boxes -->
        <div class="col-md-5">
            <div class="form-group">
                <p>First Name</p>
                <asp:TextBox ID="FirstName" runat="server" class="form-control" placeholder="First Name"></asp:TextBox>
            </div>
            <div class="form-group">
                <p>Date of birth</p>
                <asp:TextBox ID="TextBoxCalendar" runat="server" Type="Date" class="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <p>Who will see your posts? Public: everyone, Private: friends only</p>
                <asp:DropDownList ID="DropDownListPrivacy" runat="server" class="form-control">
                    <asp:ListItem Value="True">Privacy: Public</asp:ListItem>
                    <asp:ListItem Value="False">Privacy: Friends</asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="form-group">
                <!-- Change password in Bootstrap Modal-->
                <asp:ScriptManager ID="ScriptManagerModal" runat="server">
                </asp:ScriptManager>
                <div>
                    <!-- Reimport Bootstrap as it doesn't work otherwise -->
                    <!-- Bootstrap -->
                    <!-- Latest compiled and minified CSS -->
                    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
                    <!-- Optional theme -->
                    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css" />
                    <!-- Latest compiled and minified JavaScript -->
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
                    <!-- jQuery library -->
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                    <!-- Trigger the modal with a button -->

                    <!-- Modal-->
                    <button type="button" class="btn btn-info col-xs-12 col-sm-12 col-md-12" data-toggle="modal" data-target="#modalChangePswd">Change Password</button>
                    <asp:UpdatePanel ID="UpdatePanelChangePswd" runat="server">
                        <ContentTemplate>
                            <div class="modal fade" id="modalChangePswd" role="dialog">
                                <div class="modal-dialog modal-lg">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Change Password</h4>
                                        </div>

                                        <!-- User input in Modal-->
                                        <div class="modal-body col-md-12">
                                            <div class="form-group">
                                                <p>Current Password</p>
                                                <asp:TextBox ID="TextBoxCurrentPswd" class="form-control" runat="server" BorderStyle="Solid" BorderWidth="1" TextMode="Password"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                <p>New Password</p>
                                                <asp:TextBox ID="TextBoxNewpswd" class="form-control" runat="server" BorderWidth="1" BorderStyle="Solid" TextMode="Password"></asp:TextBox>
                                            </div>

                                            <div class="container">
                                                <asp:Button ID="ButtonChangePassword" class="btn btn-success" runat="server" Text="Save Changes" />
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <!-- Modal end -->
        </div>

        <!-- Further user input -->
        <div class="col-md-4">
            <div class="form-group">
                <p>Family name</p>
                <asp:TextBox ID="LastName" runat="server" class="form-control" placeholder="Last Name"></asp:TextBox>
            </div>

            <div class="form-group">
                <p>Course</p>
                <asp:DropDownList ID="DropDownListCourse" runat="server" class="form-control" DataSourceID="AccessDataSourceCourse" DataTextField="course_name" DataValueField="ID">
                    <asp:ListItem Enabled="False">Course</asp:ListItem>
                </asp:DropDownList>
                <asp:AccessDataSource ID="AccessDataSourceCourse" runat="server" SelectCommand="SELECT [course_name], [ID] FROM [courses] ORDER BY [ID] DESC" DataFile="~/App_Data/1419631.mdb"></asp:AccessDataSource>
            </div>

            <div class="form-group">
                <p>Select ALL of your interests:</p>
                <asp:ListBox ID="ListBoxInterests" class="form-control" runat="server" DataSourceID="AccessDataSourceInterests" DataTextField="interests" DataValueField="interests" SelectionMode="Multiple" onclick="ListBoxClient_SelectionChanged(this, event);"></asp:ListBox>
                <asp:AccessDataSource ID="AccessDataSourceInterests" runat="server" SelectCommand="SELECT [interests] FROM [interests]" DataFile="~/App_Data/1419631.mdb"></asp:AccessDataSource>
            </div>
        </div>
    </div>
    <!-- User Input end -->

    <!-- Submit user values-->
    <div class="row">        
        <div class="container" style="padding: 0px;">
            <div class="col-md-3"></div>
            <div class="col-sm-12 col-md-9">
                <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" Class="col-xs-12 col-sm-12 col-md-12 btn btn-success" Style="margin-left: 8px; width:825px;" />
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-9 pull-right" style="text-align: center;">
                <!-- Inform about user made changes-->
                <asp:Label ID="LabelChangePassword" runat="server" ForeColor="#CC0000"></asp:Label>
                <asp:Label ID="LabelInfoChanged" runat="server" ForeColor="#00b300"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>
