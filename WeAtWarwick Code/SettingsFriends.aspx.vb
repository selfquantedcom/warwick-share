﻿Imports System.Data.OleDb

Partial Class SettingsFriends
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Establish Db connection
        Dim myconnection = CType(Master, MasterPage).EstablishDbConn()
        myconnection.Open()

        'Establish variables, queries and commands
        Dim userID As String = CType(Master, MasterPage).GetUserID()
        Dim friendsTable As New Table
        Dim friendCond As Boolean = False
        Dim anyFriends As Boolean = False
        Dim friendID As Integer

        Dim getFriednsQry As String = "SELECT * FROM [friends];"
        Dim getFriendscmd As New OleDbCommand(getFriednsQry, myconnection)
        Dim readerFriends As OleDbDataReader = getFriendscmd.ExecuteReader()

        'Display table with user's friends 
        While readerFriends.Read()
            'Identify a friend of the current user from the table friends
            If (readerFriends(1) = userID) Then
                If (readerFriends(3) = True) Then
                    friendCond = True
                    friendID = readerFriends(2)
                End If
            End If
            If (readerFriends(2) = userID) Then
                If (readerFriends(3) = True) Then
                    friendCond = True
                    friendID = readerFriends(1)
                End If
            End If

            'Search through table users to find the friend
            If (friendCond) Then
                anyFriends = True
                Dim getFriendsInfoQry As String = "SELECT * FROM [users];"
                Dim getFriendsInfocmd As New OleDbCommand(getFriendsInfoQry, myconnection)
                Dim readerUsers As OleDbDataReader = getFriendsInfocmd.ExecuteReader()

                While readerUsers.Read()
                    If (readerUsers(0) = friendID) Then
                        'Retrieve detailed information about the friend from table users

                        Dim DataRow As New TableRow
                        DataRow.BorderWidth = 1

                        Dim profilePic As New Image
                        profilePic.Width = 80
                        profilePic.Height = 80
                        Dim detail As New Button
                        detail.CssClass = "btn btn-info"
                        Dim removeFriend As New Button
                        removeFriend.CssClass = "btn btn-danger"

                        DataRow.TableSection = TableRowSection.TableBody
                        Dim DataCell1, DataCell2, DataCell3, DataCell4, DataCell5 As New TableCell

                        'load picture
                        profilePic.ImageUrl = readerUsers(8)

                        DataCell1.Controls.Add(profilePic)
                        DataCell2.Text = readerUsers(2).ToString()
                        DataCell3.Text = readerUsers(3).ToString()

                        'display detailed info about user on btn click
                        AddHandler detail.Click, AddressOf Me.Detail_Click
                        detail.Text = "Detail"
                        detail.CommandName = readerUsers(0).ToString()
                        DataCell4.Controls.Add(detail)

                        'Remove friend - button
                        AddHandler removeFriend.Click, AddressOf Me.RemoveFriend_Click
                        removeFriend.Text = "Remove friend"
                        removeFriend.CommandName = readerUsers(0).ToString()
                        DataCell5.Controls.Add(removeFriend)

                        DataRow.Cells.Add(DataCell1)
                        DataRow.Cells.Add(DataCell2)
                        DataRow.Cells.Add(DataCell3)
                        DataRow.Cells.Add(DataCell4)
                        DataRow.Cells.Add(DataCell5)

                        'Styling
                        DataCell1.BackColor = Drawing.Color.Azure
                        DataCell2.BackColor = Drawing.Color.Azure
                        DataCell3.BackColor = Drawing.Color.Azure
                        DataCell4.BackColor = Drawing.Color.Azure
                        DataCell5.BackColor = Drawing.Color.Azure

                        friendsTable.Rows.Add(DataRow)

                        friendID = 0
                    End If
                End While
                readerUsers.Close()
            End If
        End While

        readerFriends.Close()
        myconnection.close()
        PanelFriends.Controls.Add(friendsTable)

        'Display a message when the user has no friends
        If (anyFriends = False) Then
            LabelFriends.Text = "You dont have any friends yet! <a href='Network.aspx?sessionVar=" & userID & "'>Make Friends</a>"
        End If

    End Sub

    'Handler for accept friend button
    Private Sub AcceptFriend_Click(sender As Object, e As EventArgs)
        Dim myconnection = CType(Master, MasterPage).EstablishDbConn()
        myconnection.Open()

        'Establish variables, queries and commands
        Dim userID As String = CType(Master, MasterPage).GetUserID()
        Dim btn As Button = CType(sender, Button)
        Dim friendID As Integer = Int(btn.CommandName)

        Dim updateFriendRecordQry As String = "UPDATE friends SET friend_request_acceptance = True, request_sent = False WHERE (friend_request_id = " & friendID & " AND friend_recipient_id = " & userID & ") OR (friend_recipient_id = " & friendID & " AND friend_request_id = " & userID & ");"
        Dim updateFriendRecordcmd As New OleDbCommand(updateFriendRecordQry, myconnection)
        updateFriendRecordcmd.ExecuteNonQuery()

        myconnection.Close()
        Response.Redirect(Request.RawUrl)

    End Sub

    'add friend button handler
    Private Sub AddFriend_Click(sender As Object, e As EventArgs)
        'Establish Db connection
        Dim myconnection = CType(Master, MasterPage).EstablishDbConn()
        myconnection.Open()

        'Establish variables, queries and commands
        Dim userID As String = CType(Master, MasterPage).GetUserID()
        Dim btn As Button = CType(sender, Button)

        Dim friendID As Integer = Int(btn.CommandName)
        Dim currentDate As DateTime = DateTime.Today.ToString("dd/MM/yyyy")
        Dim currentTime As DateTime = DateTime.Now.ToString("HH:mm")

        'Verify is friend record already exists in table friends
        Dim verifyEntryQry As String = "Select COUNT(*) FROM friends WHERE (friend_request_id = " & friendID & " And friend_recipient_id = " & userID & ") Or (friend_recipient_id = " & friendID & " And friend_request_id = " & userID & ");"
        Dim verifyEntrycmd As New OleDbCommand(verifyEntryQry, myconnection)

        'If so, Update record 
        If (Int(verifyEntrycmd.ExecuteScalar()) > 0) Then
            Dim updateFriendRecordQry As String = "UPDATE friends Set friend_request_id = " & userID & ", friend_recipient_id = " & friendID & ",  friend_request_acceptance = False, request_sent = True WHERE (friend_request_id = " & friendID & " AND friend_recipient_id = " & userID & ") OR (friend_recipient_id = " & friendID & " AND friend_request_id = " & userID & ");"
            Dim updateFriendRecordcmd As New OleDbCommand(updateFriendRecordQry, myconnection)
            updateFriendRecordcmd.ExecuteNonQuery()

            'Otherwise create new record
        Else 'friend_acceptance_date, friend_request_acceptance, @accDate, , @accTime)
            Dim insertFriendRecordQry As String = "INSERT INTO [friends] (friend_request_id, friend_recipient_id, friend_request_acceptance, request_sent) VALUES(@userID, @friendID, @reqAcc, @reqSent);"
            Dim insertFriendRecordcmd As New OleDbCommand(insertFriendRecordQry, myconnection)
            insertFriendRecordcmd.Parameters.AddWithValue("@userID", userID)
            insertFriendRecordcmd.Parameters.AddWithValue("@friendID", friendID)
            insertFriendRecordcmd.Parameters.AddWithValue("@reqAcc", False)
            insertFriendRecordcmd.Parameters.AddWithValue("@reqSent", True)
            insertFriendRecordcmd.ExecuteNonQuery()
        End If

        myconnection.Close()
        Response.Redirect(Request.RawUrl)
    End Sub


    'Handler for detail's button
    Private Sub Detail_Click(sender As Object, e As EventArgs)
        'Display details view
        Dim btn As Button = CType(sender, Button)
        CType(Master, MasterPage).GenerateDetailsView(PanelDetail, btn)
    End Sub

    'Remove friend button handler 
    Private Sub RemoveFriend_Click(sender As Object, e As EventArgs)
        Dim btn As Button = CType(sender, Button)
        CType(Master, MasterPage).RemoveFriend(btn)
    End Sub

    'Sub page buttons
    Protected Sub ButtonAboutYou_Click(sender As Object, e As EventArgs) Handles ButtonAboutYou.Click
        'Go to Settings page, About you subpage
        Dim userID As String = CType(Master, MasterPage).GetUserID()
        Response.Redirect("SettingsAbout.aspx?sessionVar=" & userID)
    End Sub

    Protected Sub ButtonMyFriends_Click(sender As Object, e As EventArgs) Handles ButtonMyFriends.Click
        'Go to Settings page, friends subpage
        Response.Redirect(Request.RawUrl)
    End Sub
End Class