﻿Imports System.Data.OleDb

Partial Class Newsfeed
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Establish database connection
        Dim myconnection = CType(Master, MasterPage).EstablishDbConn()
        myconnection.Open()

        'Declare variables, sql queries and commands
        Dim userID As String = CType(Master, MasterPage).GetUserID()
        Dim fName As String = ""
        Dim lName As String = ""
        Dim profilePic As String = ""

        Dim profilePicQry As String = "SELECT * FROM [users]"
        Dim profilePiccmd As New OleDbCommand(profilePicQry, myconnection)
        Dim reader As OleDbDataReader = profilePiccmd.ExecuteReader()

        'Execute Qry
        While reader.Read()
            If (reader(0).ToString() = userID) Then
                fName = reader(2).ToString()
                lName = reader(3).ToString()
                profilePic = reader(8).ToString()
            End If
        End While
        reader.Close()

        'Display profile pic, first/last name 
        ImageProfilePic.ImageUrl = profilePic
        LabelUserNameSurname.Text = fName & " " & lName


        'Hide GV rows based on privacy settings
        'Define some vars
        Dim privacyCond As Boolean = True
        Dim friendCond As Boolean = True

        'Iterate over rows
        For i As Integer = 0 To GridViewPosts.Rows.Count - 1
            Dim row = GridViewPosts.Rows(i)

            'Get identifiers from a row
            Dim postID As Integer = Int(row.Cells(0).Text.ToString())
            Dim userPosterID As Integer = Int(row.Cells(1).Text.ToString())
            privacyCond = True
            friendCond = True

            'The user's posts are automatically visible
            If (userPosterID = userID) Then
                Continue For
            End If

            'Get privacy settings status
            Dim privacySettingQry As String = "SELECT public_privacy FROM [users] WHERE ID = " & userPosterID & " ;"
            Dim privacySettingcmd As New OleDbCommand(privacySettingQry, myconnection)
            privacyCond = privacySettingcmd.ExecuteNonQuery()

            'TESTING
            privacyCond = True

            'When user setting not public then get information about frinedship status between user of a particular post and the current user
            If (privacyCond = False) Then
                Dim friendshipStatusQry As String = "SELECT friend_request_acceptance FROM [friends] WHERE (friend_request_id = " & userPosterID & " AND friend_recipient_id = " & userID & ") OR (friend_recipient_id = " & userPosterID & " AND friend_request_id = " & userID & ");"
                Dim friendshipStatuscmd As New OleDbCommand(friendshipStatusQry, myconnection)
                friendCond = friendshipStatuscmd.ExecuteNonQuery()

                'When not friends, hide the post
                If (friendCond = False) Then
                    GridViewPosts.Rows.Item(i).Visible = False
                End If
            End If
        Next



        myconnection.Close()
    End Sub

    'Handle  nested grid view
    Private Sub GridViewPosts_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridViewPosts.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ds1 As AccessDataSource = New AccessDataSource
            ds1.DataFile = "~/App_Data/1419631.mdb"
            ds1.SelectCommand = "SELECT user_id, comment, comment_date, comment_time FROM comment WHERE post_id =" & e.Row.Cells(0).Text & " ORDER BY comment_date ASC, comment_time ASC;"

            CType(e.Row.FindControl("GridViewComments"), GridView).DataSource = ds1
            CType(e.Row.FindControl("GridViewComments"), GridView).DataBind()

        End If

    End Sub

    Protected Sub ButtonPost_Click(sender As Object, e As EventArgs) Handles ButtonPost.Click

        'check for empty text box
        If (TextBoxComposePost.Text <> "") Then

            'establish database connection
            Dim myconnection = CType(Master, MasterPage).EstablishDbConn()
            myconnection.Open()

            'Establish variables, queries and commnds
            Dim insertQry As String = "INSERT INTO [posts]([post_content], [post_date], [user_id], [post_time]) VALUES(@postContent, @postDate, @usrID, @postTime);"
            Dim post_content As String = TextBoxComposePost.Text
            Dim post_date As String = DateTime.Today.ToString("dd/MM/yyyy")
            Dim post_time As String = DateTime.Now.ToString("HH:mm")
            Dim userID As Integer = Request.QueryString("sessionVar").ToString()

            Dim userscmd As New OleDbCommand(insertQry, myconnection)

            'insert user defined values into the query
            userscmd.Parameters.AddWithValue("@postContent", post_content)
            userscmd.Parameters.AddWithValue("@postDate", post_date)
            userscmd.Parameters.AddWithValue("@usrId", userID)
            userscmd.Parameters.AddWithValue("@postTime", post_time)

            userscmd.ExecuteNonQuery()
            myconnection.Close()

            'Clear inputs and refresh
            TextBoxComposePost.Text = ""
            Response.Redirect(Request.RawUrl)
        End If
    End Sub

    'Submit comment button handler 
    Protected Sub ButtonSubmitComment_Click(sender As Object, e As EventArgs)
        'Establish Db connection
        Dim myconnection = CType(Master, MasterPage).EstablishDbConn()
        myconnection.Open()

        For Each row As GridViewRow In GridViewPosts.Rows
            Dim TextBoxComment As TextBox = row.FindControl("TextBoxComment")

            If (TextBoxComment.Text <> "") Then

                Dim postID As Integer = CInt(sender.CommandArgument)
                Dim userID As String = CType(Master, MasterPage).GetUserID()
                Dim addCommentQry As String = "INSERT INTO [comment] (comment, comment_date, post_id, user_id, comment_time) VALUES (@comment, @date, @postID, @usrID, @time);"
                Dim addCommentcmd As New OleDbCommand(addCommentQry, myconnection)

                addCommentcmd.Parameters.AddWithValue("@comment", TextBoxComment.Text)
                addCommentcmd.Parameters.AddWithValue("@date", DateTime.Today.ToString("dd/MM/yyyy"))
                addCommentcmd.Parameters.AddWithValue("@postID", postID)
                addCommentcmd.Parameters.AddWithValue("@usrID", userID)
                addCommentcmd.Parameters.AddWithValue("@time", DateTime.Now.ToString("HH:mm"))

                addCommentcmd.ExecuteNonQuery()
                GridViewPosts.DataBind()

                TextBoxComment.Text = ""

            End If
        Next

        myconnection.Close()
        Response.Redirect(Request.RawUrl)
    End Sub
End Class