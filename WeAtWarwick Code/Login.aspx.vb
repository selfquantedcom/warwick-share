﻿Imports System.Data.OleDb
Imports System.Drawing

Partial Class Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Reset fields 
        TextBoxEmail.Text = ""
        TextBoxPassword.Text = ""
        TextBoxEmail.BorderColor = Color.FromArgb(204, 204, 204)
        TextBoxPassword.BorderColor = Color.FromArgb(204, 204, 204)
    End Sub

    'Handle login form
    Protected Sub Login_Click(sender As Object, e As EventArgs) Handles Me.Load

        'Establish database connection - doesnt have master page hence cant import functions from there
        Dim constring As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Request.PhysicalApplicationPath.ToString() & "/App_Data/1419631.mdb"
        Dim myconnection = New OleDbConnection(constring)
        myconnection.Open()

        'Declare variables, queries and Db commands
        Dim userExists As Integer
        Dim userID As String

        Dim verifyEmailsqlQry As String = "Select COUNT(*) FROM [users] WHERE [users].[user_email]='" & TextBoxEmail.Text & "'"
        Dim databaseStoredPassword As String = "SELECT [users].[user_password] FROM [users] WHERE [users].[user_email]='" & TextBoxEmail.Text & "'"
        Dim userIDQry As String = "SELECT [users].[ID] FROM [users] WHERE [users].[user_email]='" & TextBoxEmail.Text & "'"

        Dim verifyEmailcmd As New OleDbCommand(verifyEmailsqlQry, myconnection)
        Dim verifyPswdcmd As New OleDbCommand(databaseStoredPassword, myconnection)
        Dim userIDcmd As New OleDbCommand(userIDQry, myconnection)

        'Verify that email address has been registered
        userExists = Int(verifyEmailcmd.ExecuteScalar().ToString())

        'Verify password
        If (userExists = 1) Then
            databaseStoredPassword = verifyPswdcmd.ExecuteScalar().ToString()

            If (databaseStoredPassword = TextBoxPassword.Text) Then
                userID = userIDcmd.ExecuteScalar().ToString()

                'handle Admin login
                If (TextBoxEmail.Text = "Admin@warwick.ac.uk") Then
                    'Create session variable with user ID
                    Session(userID) = userID
                    Response.Redirect("Admin.aspx?sessionVar=" & userID) 'Store user ID in URL
                End If

                'Create session variable with user ID
                Session(userID) = userID
                Response.Redirect("Newsfeed.aspx?sessionVar=" & userID) 'Store user ID in URL

            Else 'Field validators
                TextBoxPassword.Text = ""
                TextBoxPassword.BorderColor = Color.Red
            End If
        Else
            TextBoxEmail.Text = ""
            TextBoxPassword.Text = ""
            TextBoxEmail.BorderColor = Color.Red
            TextBoxPassword.BorderColor = Color.Red
        End If

        myconnection.Close()
    End Sub
End Class